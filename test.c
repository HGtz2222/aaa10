// 命令行版本的进度条程序
#include <stdio.h>
// 只在 Linux 下才有这个文件
#include <unistd.h>

// cache 缓存  vs  buffer 缓冲区
int main() {
  // # 表示进度条
  const char* label = "/-\\|";
  char buf[1024] = {0};
  int i = 0;
  for (; i < 100; ++i) {
    buf[i] = '#';
    // \n 表示换行, 另起一个新行, 光标来到行首
    // \r 表示回车, 不另起新行, 光标来到行首
    printf("[%d%%][%c][%s]\r", i, label[i % 4], buf);
    fflush(stdout);
    // 单位是 秒
    usleep(100 * 1000);
  }
  
  return 0;
}
